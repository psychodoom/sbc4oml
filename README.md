# SBC OML para Cloud

SO: Ubuntu 18.04

Codigo de asterisk para el componente SBC el cual se usa como SIP gateway entre Asterisk OML de todos los clientes y la PSTN.

Para mas información del código de asterisk y de la configuración manual de los componentes, buildear los .rst del folder `/doc`.

## Seteando la cuenta AWS en la maquina que ejecutará el script

Verificar la seccion "Seteando la cuenta AWS en la maquina que ejecutará el deploy" del repositorio ftsaas

## Exportar configuración del sbc a bucket s3

Si por algun motivo se necesita subir al bucket la configuración de asterisk del SBC se puede hacer con el siguiente comando, dentro de la instancia de SBC con el usuario root:
```sh
  aws s3 sync /opt/asterisk/etc/ s3://oml-prod-shared-astsbc-configuration
  aws s3 cp /opt/asterisk/var/lib/asterisk/astdb.sqlite3 s3://oml-prod-shared-astsbc-configuration
```

Este ejemplo sube toda la carpeta asterisk que contiene todos los archivos de configuración. Si se desea exportar otra cosa, basta con cambiar el path o archivo de origen.

## Script sbc_configuration.sh

Este script se usa para desplegar la configuración tenant/trunk o outr de un cliente, escribiendo en todas las instancias de AstSBC que se encuentren prendidas.  Es necesario tener seteado AWS en la máquina donde se correrá el script:

1. Primero, ingresar en el script y observar las variables iniciales, setear las variables según lo que se quiera configurar:
```sh
  #Parámetros para deploy_tenant.sh
  CUSTOMER='felipem' # Nombre del tenant a configurar se usa tanto para outr como para deploy tenant
  TRUNK='freepbx' # Nombre de la troncal PSTN a insertar
  TRUNK_PORT='6066' # Puerto SIP de la troncal PSTN, si se deja vacia se toma 5060 por defecto
  TRUNK_IP='201.235.179.118' # IP/fqdn de la troncal PSTN
  TRUNK_USER='felo-centos' # User para autenticacion SIP con la troncal PSTN, si se deja vacia no hará autenticacion
  TRUNK_PASSWORD='Password$' # Password para autenticacion SIP con la troncal PSTN, si se deja vacia no hará autenticacion

  #Parametros para outr-configuration.py
  TRUNKS=('freepbx') # Troncales para failover en la outr, agregar las troncales que se quieran, entre comillas simples, el orden que se ponga aca sera el orden de failover
  PATTERN='_X.' # Pattern para la ruta saliente
  NAME='todo' # Nombre que tendrá la ruta saliente
  DIAL_TIMEOUT='' # Dial timeout para la ruta saliente, si se deja vacio toma 60 seg por defecto
  PREFIX='' # Prefix para la ruta saliente. se puede dejar vacio si no se necesita prefix en la outr
  PREPEND='' # Prepend para la ruta saliente. se puede dejar vacio si no se necesita prend en la outr
  DIAL_OPTIONS='' # Dial options para la outr, si se deja vacio toma Tt por defecto
```

2. Una vez seteado, ejecutar el script, el cual puede tener dos opciones:

- Si se quiere deployar tenant/trunk:
```sh
  $ ./sbc_configuration.sh --deploy_tenant
```
```sh
- Si se quiere configurar outr:
  $ ./sbc_configuration.sh --outr_configuration
```

Este script invoca por abajo los scripts que se mencionan a continuación:

## Script deploy_tenant.sh

Este script va a escribir la configuración del sbc_pjsip_endoints_omls.conf y sbc_pjsip_endoints_outside.conf ademas de setear las entradas correspondientes en AstDB. Para ver las opciones que toca pasarle, remitirse al help del script:
```sh
    $ ./deploy_tenant.sh --help

        AstSBC tenant/trunk deploy script
        How to use it:
              --customer=$CUSTOMER the name of the tenant to deploy
              --trunk=$TRUNK the name of the trunk to add
              --trunk_ip=$TRUNK_IP the IP/fqdn of the the trunk to add
              --trunk_port=$TRUNK_PORT  the SIP port of the trunk to add. If not passed default port added is 5060
              --trunk_user=$TRUNK_USER  the SIP user of the trunk to add.
              --trunk_password$TRUNK_PASSWORD the SIP password of the trunk to add.
```
Por ejemplo,
```sh
  $ ./deploy_tenant.sh --trunk_ip=201.235.179.118 --customer=fabuntu --trunk=freepbx --trunk_port=6066 --trunk_user=felo-centos --trunk_password=Password$
```

## Script outr_configuration.py

Este script es para añadir una ruta saliente por cliente. Va a escribir la configuración de ruta saliente en el archivo sbc_extensions_from_outside.conf y va a setear las entradas en AstDB para dicha ruta. Para ver las opciones que toca pasarle, remitirse al help del script:
```sh
    $  python3 outr_configuration.py --help
usage: outr_configuration.py [-h] [--customer CUSTOMER] [--pattern PATTERN]
                             [--name NAME] [--dial_options DIAL_OPTIONS]
                             [--dial_timeout DIAL_TIMEOUT] [--prefix PREFIX]
                             [--prepend PREPEND] [--trunk TRUNK]

SBC Outr configuration script

optional arguments:
  -h, --help            show this help message and exit
  --customer CUSTOMER   The tenant that will own the outr
  --pattern PATTERN     The pattern for this outr
  --name NAME           The name that will have this outr
  --dial_options DIAL_OPTIONS
                        The dial options for this route, this option is not
                        mandatory
  --dial_timeout DIAL_TIMEOUT
                        The dial timeout for this route, this option is not
                        mandatory
  --prefix PREFIX       Prefix for this route, this option is not mandatory
  --prepend PREPEND     Prepend for this route, this option is not mandatory
  --trunk TRUNK         Specifies the trunk or trunks for this route. You can
                        insert more than one trunk, the order you insert the
                        trunks will be the order of failover
```
Por ejemplo,
```sh
  $  python3 outr_configuration.py --customer fabuntu --pattern ZZ. --name celulares --trunk fabuntu-freepbx --trunk fabuntu-iplan
```
