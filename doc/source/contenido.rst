.. _Descrpición general:

************
Introducción
************

.. image:: img/sbc4oml.png

aaaaaaaaaaaaaaaaaaaaaaaa

.. _Dialplan & AstDB:

****************
Dialplan & AstDB
****************



.. _Puertos, NAT y direcciones IP:

*****************************
NAT, puertos y direcciones IP
*****************************


.. _Seguridad:

*********
Seguridad
*********


.. _Alta de Instancia de OML:


********************************
Configuración de IP publica SIP
********************************

Editar el archivo *sbc_pjsip_transports.con* para descomentar e insertar la dirección IP pública con la que salen nateados los paquetes SIP y SDP.

external_media_address=
external_signaling_address=


************************
Alta de Instancia de OML
************************

Para dar de alta un nuevo Tenant-OML debemos trabajar a nivel archivo de configuración (sbc_pjsip_endpoints_omls.conf).


.. image:: img/sbc-tenant-trunk.png

Archivo sbc_pjsip_endpoints_omls.conf
**************************************

Para generar un nuevo Tenant debemos replicar la plantilla expuesta a continuación.

.. code-block:: bash

 [$NOMBRE_TENANT](omnileads_endpoints)
 inbound_auth/username=$NOMBRE_TENANT
 inbound_auth/password=C11H15NO2-C12h17N204P
 endpoint/context=from-$NOMBRE_TENANT
 endpoint/set_var=TENANT=$NOMBRE_TENANT
 endpoint/direct_media=no

Como se resalta, el único parámetro variable es **$NOMBRE_TENANT** que aparece 3 veces en la plantilla. Ponga el nombre que desee el Asterisk admin.
Para que se tomen los cambios ejecutar:

.. code-block:: bash

 asterisk -rx 'pjsip reload'


.. _Alta de troncales para cada instancia de OML:


Alta del trunk en el Tenant
***************************

Para ello simplemente levantar un nuevo Trunk PJSIP "Custom" en la interfaz de OMniLeads.
Luego utilizar la siguiente plantilla para levantar el troncal.

.. code-block:: bash

 type=wizard
 transport=trunk-transport
 accepts_registrations=no
 accepts_auth=no
 sends_registrations=yes
 sends_auth=yes
 aor/qualify_frequency=60
 endpoint/rtp_symmetric=no
 endpoint/force_rport=no
 endpoint/rewrite_contact=yes
 endpoint/timers=yes
 endpoint/allow=alaw,ulaw
 endpoint/dtmf_mode=rfc4733
 endpoint/context=from-pstn
 endpoint/from_user=$NOMBRE_TENANT
 outbound_auth/username=$NOMBRE_TENANT
 outbound_auth/password=C11H15NO2-C12h17N204P
 remote_hosts=XXX.XXX.XXX.XXX:6060


Donde: **XXX.XXX.XXX.XXX** es la IP del SBC dentro de la red LAN.

********************************************
Alta de troncales para cada instancia de OML
********************************************

Para esto se deben realizar dos acciones, por un lado trabajar sobre el archivo sbc_pjsip_endpoints_outside.conf y por el otro a nivel de AstDB.

.. image:: img/sbc-pstn-trunk.png

Archivo sbc_pjsip_endpoints_outside.conf
****************************************

Como se conoce cada tenant puede tener más de un trunk de acceso a la PSTN. Aquí vamos a considerar los pasos para dar de alta un trunk pstn.
Para ello también se parten de diferentes plantillas.

.. code-block:: bash

 [$TENANT_PROVIDER_NAME](outside_endpoints)
 accepts_registrations=no
 accepts_auth=no
 sends_auth=yes
 sends_registrations=yes
 endpoint/force_rport=yes
 endpoint/rtp_symmetric=yes
 endpoint/send_rpid=yes
 endpoint/send_pai=yes
 aor/authenticate_qualify=no
 endpoint/set_var=TENANT=$NOMBRE_TENANT
 ;remote_hosts=XXX.XXX.XXX.XXX:PPPP
 ;endpoint/permit=XXX.XXX.XXX.XXX/32
 ;endpoint/from_user=$USERNAME
 ;outbound_auth/username=$USERNAME
 ;outbound_auth/password=$PASSWORD


* Los primeros 2 parámetros hacen referencia a si queremos activar la registración desde el otro extremo hacia nosotros y si vamos a solicitar autenticación a las llamadas que ingresen desde el otro extremo, respectivamente.

* Los segundos 2 parámetros son similares / opuestos. Es decir que los conceptos a aplican desde nosotros hacia el SIP-Server del otro extremo.

* Luego tenemos al parámetro (set_var=TENANT) que debe llevar el nombre del Tenant (ya utilizado en el paso anterior).

* Luego tenemos al par *IP* o *Hostname.domain* junto al puerto *UDP* , para indicar la ubicación del SIP-Server con el cual armar el trunk.

* En caso de tener habilidata la autenticación entrante o saliente, se deben indicar los parámetros correspondientes.


Cargar valores AstDB
********************

Cada vez que se asigna un nuevo troncal a un tenant se deben realizar las siguientes acciones a nivel AstDB

.. code-block:: bash

 asterisk -rx 'database put SBC/TENANT/$TENANTID TRUNKS $CANTIDAD'
 asterisk -rx 'database put SBC/TENANT/$TENANTID/TRUNK/$NUM NAME $NOMBRE_TRUNK_PJSIP'

* Donde **$CANTIDAD** hace referencia a la cantidad de Trunks que tiene asociado el tenant.
* **$NOMBRE_TRUNK_PJSIP** hace referencia al nombre de que se asignó a este trunk en el archivo sbc_pjsip_endpoints_outside.conf.


*********************************
Alta de ruta saliente para Tenant
*********************************

Antes de avanzar en la configuración se define lo que es una Ruta Saliente.

Cada Tenant puede contar con "n" rutas salientes y cada ruta saliente puede involucrar a más de un Trunk a la hora de procesar llamadas y tratar de
encaminarlas hacia la PSTN.

Archivo sbc_extensions_to_outside.conf
**************************************

Cada ruta saliente en términos de archivo de configuración implica un "Pattern Match" de dialplan. Simplemente hay que generar el patrón de discado utilizando
como plantilla las tres lineas expuestas.


Cada ruta saliente en términos de archivo de configuración implica un "Pattern Match" de dialplan. Simplemente hay que generar el patrón de discado utilizando
como plantilla las tres lineas expuestas.

.. code-block:: bash

 [from-tenantx]
 exten => $PATTERN,1,Verbose(Ruta $ID para tenant ${CONTEXT})
 same => n,Gosub(sbc-to-outside,s,1(${TENANT},$ID,${EXTEN}))
 same => n,Hangup()

Donde:

* **$PATTERN** hace alusión al patrón de discado utilizado para representar los tipos de llamadas a cursar por la ruta.
* **$ID** es el número de ruta asociado al Tenant. (Simplemente se empieza con 1 hasta n)

Veamos un ejemplo de un Tenant con 3 rutas salientes.

.. code-block:: bash

 [from-tenantx]
 exten => _X.,1,Verbose(Ruta 1 para tenant ${CONTEXT})
 same => n,Gosub(sbc-to-outside,s,1(${TENANT},1,${EXTEN}))
 same => n,Hangup()

 exten => _00XXXXXXXX,1,Verbose(Ruta 2 para tenant ${CONTEXT})
 same => n,Gosub(sbc-to-outside,s,1(${TENANT},2,${EXTEN}))
 same => n,Hangup()

 exten => _77XXXXXXXX,1,Verbose(Ruta 3 para tenant ${CONTEXT})
 same => n,Gosub(sbc-to-outside,s,1(${TENANT},3,${EXTEN}))
 same => n,Hangup()


Cargar valores AstDB
********************

Luego de dar de alta el códido inherente al dialplan, se procede con la inserción de entradas referentes a la nueva ruta en AstDB.

Vamos a explicarlo con un ejemplo, supongamos que ruta va a utilizar:

* tT como opciones para el Dial()
* 60 segundos como tiempo para el DIal()
* El nombre de la ruta será 'fijos'
* El prefijo 00
* El prepend 9
* La ruta va a utilizar 2 trunks en failover
* El trunk 1 (osea el primero a intentar sacar la llamadas) será el IDTRUNK 2
* El trunk 2 (osea el failover del 1) será el IDTRUNK 1

Las inserciones para implementar lo descripto serían:

.. code-block:: bash

 asterisk -rx 'database put SBC/TENANT/$TENANTID/OUTR/$ID DIALOPTIONS tT'
 asterisk -rx 'database put SBC/TENANT/$TENANTID/OUTR/$ID DIALTIME 60'
 asterisk -rx 'database put SBC/TENANT/$TENANTID/OUTR/$ID NAME fijos'
 asterisk -rx 'database put SBC/TENANT/$TENANTID/OUTR/$ID PREFIX 00'
 asterisk -rx 'database put SBC/TENANT/$TENANTID/OUTR/$ID PREPEND 9'
 asterisk -rx 'database put SBC/TENANT/$TENANTID/OUTR/$ID TRUNKS 2'
 asterisk -rx 'database put SBC/TENANT/$TENANTID/OUTR/$ID/TRUNK 1 2'
 asterisk -rx 'database put SBC/TENANT/$TENANTID/OUTR/$ID/TRUNK 2 1'

Toda ruta debería tener al menos un Trunk y un nombre. El resto de los parámetros pueden no insertarse, osea solamente se utilizarán si hacen falta.


.. _Logs:

****
Logs
****

Los logs del sistema son generados por el llamado al AGI (sbc_agi.sh). El archivo en cuestión se ubica en:

........................

Formato del archivo
*******************

*
